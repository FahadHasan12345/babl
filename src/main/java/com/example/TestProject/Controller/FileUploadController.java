package com.example.TestProject.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.TestProject.Model.Data;
import com.example.TestProject.Repository.DataRepository;
import com.example.TestProject.Service.DataService;
import com.example.TestProject.Service.FileUploadService;
import com.example.TestProject.Service.Show;

import jxl.read.biff.BiffException;

@RestController
public class FileUploadController {
	
	@Autowired
	DataRepository dRepo;	
	FileUploadService fileUploadService=new FileUploadService();
	Show show=new Show();
	DataService dataService=new DataService();
   
	@RequestMapping(value="/upload",method = RequestMethod.POST)
	public ModelAndView uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,HttpServletRequest request) throws IllegalStateException, IOException, BiffException {
		
		if (file.isEmpty()) {
			return  new ModelAndView("application");
		}else {
	
			fileUploadService.fileUpload(file);
			List<Data> data = new ArrayList<Data>();
			data=dataService.saveData(file);
			dRepo.saveAll(data);
			
			//List<Data> data1 =show.findAll();

//			data1=(List<Data>) show.findAll();		
//            
//            request.setAttribute("allData",data1);
            ModelAndView mv=new ModelAndView("output");
		return  mv;
		}
			
	}

}
