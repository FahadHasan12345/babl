package com.example.TestProject.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.example.TestProject.Model.User;
import com.example.TestProject.Repository.UserRepository;

@Controller
public class UserController {
	
	@Autowired
	UserRepository userRepo;
	User user=new User();
	@RequestMapping("/registerUser")
	public String insertUser(User user) {
		userRepo.save(user);
		return "login";
	}
	
	
	@RequestMapping("/registration")
	public String registration() {
		
		return "registration";
	}
	
	@RequestMapping("/")
	public String login() {
		
		return "login";
	}
	
	@RequestMapping("/check")
	public ModelAndView check(String uEmail, String uPassword) {
		
		if((uEmail.equals(null)) || (uEmail.equals(null))) {
			ModelAndView mv=new ModelAndView();
		    String flag="Error";
			mv.addObject(flag, "flag");
		    mv.setViewName("login");
		    System.out.println("Null input");
			return mv;
		}
		User user =userRepo.findByuEmail(uEmail);
		if(user.getUPassword()!=null&&user.getUPassword().equals(uPassword)) {
			ModelAndView mv=new ModelAndView("application");
			System.out.println("pass match");
			return mv;	
		}
		else {
			ModelAndView mv=new ModelAndView();
		    String flag="Error";
			mv.addObject(flag, "flag");
		    mv.setViewName("login");
		    System.out.println("pass dont match");
			return mv;
		}
	    
	}
	
}

