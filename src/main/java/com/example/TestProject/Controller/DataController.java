package com.example.TestProject.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DataController {

	@RequestMapping("/output")
	public String registration() {
		
		return "output";
	}
	@RequestMapping("/a")
	public String application() {
		
		return "application";
	}
}
