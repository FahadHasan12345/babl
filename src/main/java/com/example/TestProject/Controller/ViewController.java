package com.example.TestProject.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.example.TestProject.Service.Show;


@Controller
public class ViewController {


	@Autowired
	Show show;
	
	@RequestMapping(value="/show",method = RequestMethod.GET)
	public ModelAndView uploadFile(){
	
            ModelAndView mv=new ModelAndView("show","allData",show.findAll());
		return  mv;
		}
			
	}

