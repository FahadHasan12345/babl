package com.example.TestProject.Service;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.TestProject.Model.Data;

@Service
public class DataService {
	
	public List<Data> saveData(MultipartFile file ) throws IllegalStateException, IOException {
		 String holdId=null;
		 String holdDate=null;
		 Map<String,String> map=new HashMap<String,String>();  
		 Map<String,Integer> map1=new HashMap<String,Integer>();
		 
		 int consecutiveId=1;
		 List<Data> allData = new ArrayList<Data>();
		
	    XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
	    XSSFSheet worksheet = workbook.getSheetAt(0);
	    
	    for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {
	            
	        XSSFRow row = worksheet.getRow(i);
	        DataFormatter formatter = new DataFormatter();
	        Data data=new Data();
	        
	        String day=getDay(formatter.formatCellValue(row.getCell(1)));
	        String shift=getShift(formatter.formatCellValue(row.getCell(3)));
	        String ShiftName=day+"-"+shift;
	        
	        data.setId(formatter.formatCellValue(row.getCell(0)));
	        data.setDate(formatter.formatCellValue(row.getCell(1)));
	        data.setName(formatter.formatCellValue(row.getCell(2)));
	        data.setStartTime(formatter.formatCellValue(row.getCell(3)));
	        data.setEndTime(formatter.formatCellValue(row.getCell(4)));
	        
	        if(holdId==null) {
	        	holdId=data.getId();
	        	holdDate=data.getDate();
	        	map.put(data.getId(),data.getDate()); 
	        	map1.put(data.getId(),1); 
	        	data.setShiftNAme(ShiftName);
	        	
	        }else {
	        	if(holdId.equals(data.getId())&&holdDate.equals(data.getDate())) {
	        		consecutiveId+=1;
	        		System.out.println(consecutiveId);
	        		if(consecutiveId==3) {
	        			data.setShiftNAme("Not possible:Employee can not work more than 16h a day.");
	        		}
	        		else {
	        			data.setShiftNAme(ShiftName);
	        		}
	        	}else if(holdId.equals(data.getId())){
	        		if(getDate(holdDate)+1==getDate(data.getDate())) {
	        			map1.put(holdId,map1.get(holdId)+1);
	        			if(map1.get(holdId)==7) {
	        				data.setShiftNAme("Not Possible:Employee can not work more than 6 days consecutively.");
	        			}
	        			else{
	        				holdId=data.getId();
	    		        	holdDate=data.getDate();
	    		        	consecutiveId=1;
	    		        	data.setShiftNAme(ShiftName);
	        			}
	        			
	        		}
	        		else {
	        			holdId=data.getId();
    		        	holdDate=data.getDate();
    		        	map.put(data.getId(),data.getDate()); 
    		        	map1.put(data.getId(),1); 
    		        	consecutiveId=1;
    		        	data.setShiftNAme(ShiftName);
	        		}
	        	
	        	}else {
	        		holdId=data.getId();
		        	holdDate=data.getDate();
		        	map.put(data.getId(),data.getDate()); 
		        	map1.put(data.getId(),1);
		        	consecutiveId=1;
		        	data.setShiftNAme(ShiftName);
		        	
		        	
	        	}
	        	
	        }
	        

	       allData.add(data);      
	         
	    }
	    workbook.close();
	    return allData;
	}
	
	String getDay(String date) {
		String[] words=date.split("/");
		int d=Integer.parseInt(words[1]);
		switch(d%7) {
		  case 1:
			  return "Saturday";
		  case 2:
			  return "Sunday";
		  case 3:
			  return "Monday";
		  case 4:
			 return "Tuesday";
		  case 5:
			 return "Wednesday";
		  case 6:
			 return "Thursday";
		  case 0:
			return "Friday";    
		}
         return "";
	}
	String getShift(String time) {
		
		switch(time) {
		case "8:00:00 AM":
				return "Morning";
		case "2:00:00 PM":
				return "Evening";
		case "9:00:00 PM" : 
				return "Night";
		}
		return "bad";
		
	}
	int getDate(String date) {
		String[] words=date.split("/");
		int d=Integer.parseInt(words[1]);
		return d;
	}
	
}


