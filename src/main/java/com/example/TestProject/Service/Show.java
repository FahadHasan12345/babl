package com.example.TestProject.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.TestProject.Model.Data;
import com.example.TestProject.Repository.DataRepository;


@Service
public class Show {
	@Autowired
	DataRepository dRepo;
 
//	public Iterable<Data> find() {
//		return dRepo.findAll();
//	}
		
	
public List<Data> findAll(){
	
	List<Data> allData=new ArrayList<Data>();
	for(Data data:dRepo.findAll()) {
	
		allData.add(data);
	}
	return allData;
	}
}
