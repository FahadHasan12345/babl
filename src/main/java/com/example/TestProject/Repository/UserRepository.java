package com.example.TestProject.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.TestProject.Model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	
	User findByuEmail(String uEmail);
}
