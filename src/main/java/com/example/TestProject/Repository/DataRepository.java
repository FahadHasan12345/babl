package com.example.TestProject.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.TestProject.Model.Data;

public interface DataRepository extends JpaRepository<Data, Long>{

	
	
}

