package com.example.TestProject.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long uID;
	private String uEmail;
	private String uName;
	private int uAge; 
	
	private String uPassword;
	private String uGender;
	public String getuGender() {
		return uGender;
	}
	public void setuGender(String uGender) {
		this.uGender = uGender;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public int getUAge() {
		return uAge;
	}
	public void setUAge(int uAge) {
		this.uAge = uAge;
	}
	public String getUEmail() {
		return uEmail;
	}
	public void setUEmail(String uEmail) {
		this.uEmail = uEmail;
	}
	public String getUPassword() {
		return uPassword;
	}
	public void setUPassword(String uPassword) {
		this.uPassword = uPassword;
	}
	
}

