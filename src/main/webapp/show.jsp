<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
       <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>      
   <%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %> 
<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
  }
  .status[data-status="Not possible:Employee can not work more than 16h a day."]:after {
 content : "Not possible:Employee can not work more than 16h a day.";
 color : red
}

.status[data-status="Not Possible:Employee can not work more than 6 days consecutively."]:after {
 content : "Not Possible: Worker can not work more than 6 days consecutuvely.";
 color : red
}
.status[data-status="Saturday-Morning"]:after {
 content : "Saturday-Morning";
 color : green
}
.status[data-status="Saturday-Evening"]:after {
 content : "Saturday-Evening";
 color : green
}
.status[data-status="Saturday-Night"]:after {
 content : "Saturday-Night";
 color : green
}
.status[data-status="Sunday-Morning"]:after {
 content : "Sunday-Morning";
 color : green
}
.status[data-status="Sunday-Evening"]:after {
 content : "Sunday-Evening";
 color : green
}
.status[data-status="Sunday-Night"]:after {
 content : "Sunday-Night";
 color : green
}
.status[data-status="Monday-Morning"]:after {
 content : "Monday-Morning";
 color : green
}
.status[data-status="Monday-Evening"]:after {
 content : "Monday-Evening";
 color : green
}
.status[data-status="Monday-Night"]:after {
 content : "Monday-Night";
 color : green
}
.status[data-status="Tuesday-Morning"]:after {
 content : "Tuesday-Morning";
 color : green
}
.status[data-status="Tuesday-Evening"]:after {
 content : "Tuesday-Evening";
 color : green
}
.status[data-status="Tuesday-Night"]:after {
 content : "Tuesday-Night";
 color : green
}
.status[data-status="Wednesday-Morning"]:after {
 content : "Wednesday-Morning";
 color : green
}
.status[data-status="Wednesday-Evening"]:after {
 content : "Wednesday-Evening";
 color : green
}
.status[data-status="Wednesday-Night"]:after {
 content : "Wednesday-Night";
 color : green
}
.status[data-status="Thursday-Morning"]:after {
 content : "Thursday-Morning";
 color : green
}
.status[data-status="Thursday-Evening"]:after {
 content : "Thursday-Evening";
 color : green
}
.status[data-status="Thursday-Night"]:after {
 content : "Thursday-Night";
 color : green
}
.status[data-status="Friday-Morning"]:after {
 content : "Friday-Morning";
 color : green
}
.status[data-status="Friday-Evening"]:after {
 content : "Friday-Evening";
 color : green
}
.status[data-status="Friday-Night"]:after {
 content : "Friday-Night";
 color : green
}



</style>
</head>
<body>
     <h1>Check Out Your Schedule</h1>
<table id="customers">
<thead>
  <tr>
    <th>Employee ID</th>
    <th>Name</th>
    <th>Date</th>
    <th>Start Time</th>
    <th>End Time</th>
    <th>Shift</th>
  </tr>
  </thead>
  <tbody>
  <c:forEach var="datas" items="${allData}">
   <tr>
    <td>${datas.id}</td>
    <td>${datas.name}</td>
    <td>${datas.date}</td>
    <td>${datas.startTime}</td>
    <td>${datas.endTime}</td>
    <td data-status="${datas.shiftName}" class="status"></td>
  </tr>
  </c:forEach>
  
  </tbody>
  
 
</table>

</body>
</html>
